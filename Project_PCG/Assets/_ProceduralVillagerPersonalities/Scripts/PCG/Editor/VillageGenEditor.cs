using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (VillageGenerator))]
public class VillageGenEditor : Editor {

    [Header ("Base")]

    SerializedProperty villageGenValues;
    VillageGenerator villageGenerator;
    SerializedObject soTarget;

    int currentTab;

    [Header ("Properties for Object")]
    SerializedProperty op_People;
    SerializedProperty small_Villages;
    SerializedProperty tower_Villages;
    SerializedProperty strange_Villages;

    [Header ("Properties for Probability")]
    SerializedProperty villageProbList;

    private void OnEnable () {
        // target
        villageGenerator = (VillageGenerator) target;
        soTarget = new SerializedObject (target);

        // properties
        op_People = soTarget.FindProperty ("op_People");
        small_Villages = soTarget.FindProperty ("small_Villages");
        tower_Villages = soTarget.FindProperty ("tower_Villages");
        strange_Villages = soTarget.FindProperty ("strange_Villages");

        villageProbList = soTarget.FindProperty ("villageValues");

    }
    public override void OnInspectorGUI () {

        serializedObject.Update ();

        GUILayout.Label ("Generates a grid of a specified size.");

        {
            EditorGUILayout.BeginHorizontal (EditorStyles.helpBox);

            if (GUILayout.Button ("Generate Grid")) {
                villageGenerator.Generate ();
            }

            EditorGUILayout.EndHorizontal ();

        }

        {
            EditorGUILayout.BeginHorizontal ();

            villageGenerator.gridX = EditorGUILayout.IntField ("GridX", villageGenerator.gridX);
            villageGenerator.gridZ = EditorGUILayout.IntField ("GridZ", villageGenerator.gridZ);

            EditorGUILayout.EndHorizontal ();
        }

        EditorGUILayout.Space ();

        GUILayout.Label ("Generates Villages with villagers to accompany them.");

        EditorGUILayout.BeginHorizontal (EditorStyles.helpBox);

        if (GUILayout.Button ("Generate Villages")) {
            villageGenerator.SpawnVillages ();
        }

        {
            EditorGUILayout.EndHorizontal ();

            EditorGUILayout.BeginHorizontal ();

            villageGenerator.people = EditorGUILayout.IntField ("NPC Count (Per Village)", villageGenerator.people);
            villageGenerator.villages = EditorGUILayout.IntField ("Village Count", villageGenerator.villages);

            EditorGUILayout.EndHorizontal ();
        }
        EditorGUILayout.Space ();

        {
            EditorGUILayout.BeginVertical ();

            villageGenerator.spawnSmall = EditorGUILayout.Toggle ("Spawn Small Villages", villageGenerator.spawnSmall);
            villageGenerator.spawnTower = EditorGUILayout.Toggle ("Spawn Tower Villages", villageGenerator.spawnTower);
            villageGenerator.spawnStrange = EditorGUILayout.Toggle ("Spawn Strange Villages", villageGenerator.spawnStrange);

            EditorGUILayout.EndVertical ();

        }

        EditorGUILayout.Space ();
        EditorGUILayout.Space ();

        {
            GUILayout.Label ("Clears world of villages and NPC's.");

            EditorGUILayout.BeginHorizontal (EditorStyles.helpBox);

            if (GUILayout.Button ("Clear All")) {
                villageGenerator.ClearVillages ();
            }
            EditorGUILayout.EndHorizontal ();

            EditorGUILayout.Space ();
            EditorGUILayout.Space ();
            EditorGUILayout.Space ();

            currentTab = GUILayout.Toolbar (currentTab, new string[] { "Objects", "Probability" });

            EditorGUILayout.BeginHorizontal (EditorStyles.helpBox); {

                EditorGUILayout.Space ();
                EditorGUILayout.Space ();

                switch (currentTab) {
                    case 0:

                        EditorGUILayout.BeginVertical ();

                        EditorGUILayout.PropertyField (op_People);
                        EditorGUILayout.PropertyField (small_Villages);
                        EditorGUILayout.PropertyField (tower_Villages);
                        EditorGUILayout.PropertyField (strange_Villages);

                        EditorGUILayout.EndVertical ();
                        break;

                    case 1:

                        EditorGUILayout.BeginVertical ();

                        EditorGUILayout.PropertyField (villageProbList);
                        
                        EditorGUILayout.EndVertical ();

                        break;

                }

            }

            EditorGUILayout.EndHorizontal ();

            //  base.OnInspectorGUI ();

        }
    }
}