Procedurally Generated NPC Traits

This project mainly focuses on the procedural generation of the following:

*NPC Personality Traits
*NPC's Homelands.
*NPC's Favourite Dishes/Prefered Dishes. (Mainly based on Medieval Cook, but is subject to change)
*Small Backstories for NPC's.

**How it works: **

The algorithm generates a procedural grid with numbers specified in variables GridX & GridY on the VillageGenerator script.
After generating a grid, the player/user can select a number of villages and villagers to generate within the environment.
Villages and Villagers will be spread out based on the grid's given values.
Generated NPC's will then be given random traits, as has been listed above.

**What the user can change: **

* Grid Size
* Village & Villager frequency
* All villager traits in the VillagerSO script (Based on Enums that get selected randomly in lists).

_To add or remove specific traits of a villager in VillagerSO Script : _

1. Open Villager SO script and add or remove enum value.
2. Go to SO folder and click on the villager scriptable object to show it in the inspector.
3. In the inspector , open the list that you have changed the enum value of.
4. Add a new item to the list and select the enum you have made OR remove the item which you have removed within the VillagerSO script.

You're done!
Enjoy the tool.