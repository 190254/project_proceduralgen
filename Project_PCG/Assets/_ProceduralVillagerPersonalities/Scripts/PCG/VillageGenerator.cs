using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Ref to mesh filter for visualisation of grid, if needed.

public enum VillageType {

    // enum for different types of villages
    small,
    towerVillage,
    strangeVillage
}

[System.Serializable]
public class VillageProbability {

    // Class contains all variables for probability functionality
    public VillageType villageType;
    public float probability;
    public float probabilityIncrement;

    [Header ("Debug")]

    public float probabilityLikelinessOffset;
    public float rollCount;
}

public class VillageGenerator : MonoBehaviour {

    [Header ("Values")]
    public int gridX;
    public int gridZ;
    public int people;
    public int villages;
    Vector3[] vertices;

    // Lists to control despawning 

    List<SmallVillage> smallVillageCount = new List<SmallVillage> ();
    List<TowerVillage> towerVillageCount = new List<TowerVillage> ();
    List<StrangeVillage> strangeVillageCount = new List<StrangeVillage> ();
    List<VillagerTraits> peopleCount = new List<VillagerTraits> ();

    // Village Control

    public bool spawnSmall = false;
    public bool spawnTower = false;
    public bool spawnStrange = false;

    [Header ("Object References")]

    // Object Pools

    public ObjectPoolVillagers op_People;
    public ObjectPoolVillages small_Villages;
    public ObjectPoolTowerVillages tower_Villages;
    public ObjectPoolStrangeVillages strange_Villages;

    // end object pools

    Mesh mesh;

    // Variables to control village probability for spawning

    [Header ("Village Probability")]
    public List<VillageProbability> villageValues = new List<VillageProbability> ();
    float totalProbability;
    string returnedVillage;
    bool villageSpawned = false;

    public void Generate () {

        // Ref mesh filter to spawn triangles if needed (marching squares algorithm)
        mesh = GetComponent<MeshFilter> ().mesh = new Mesh ();
        mesh.name = "Procedural Grid";

        // Generate grid of specified size using vertices.
        // Vertices can be used to spawn objects in specific areas of the grid

        //? Reason for iterating through 3 seprate values (understanding the strange for loop):
        /*
         X and Z are the iterators used in order to spawn the vertices of the grid but the position of these vertices
        are changed through using the i iterator.

        So i essentially iterates through both the X and Z values in order to create new Vector3 points for the grid by use of 
        invisible vertices. 

        Take this for example - If only x and z were used , then empty game objects would have to take the place of the vertices,
        which would greatly impact performance. (To my understanding based on the research I've done and tutorials I've seen)

        */

        vertices = new Vector3[(gridX + 1) * (gridZ + 1)];
        for (int i = 0, z = 0; z <= gridZ; z++) {
            for (int x = 0; x <= gridX; x++, i++) {
                vertices[i] = new Vector3 (x, 0, z);

            }

        }

    }

    public void SpawnVillages () {
        ClearVillages ();

        // Generates Villages at randomly selected locations within the generated grid 

        for (var i = 0; i < villages; i++) {

            SpawnSelectedVillage ();
        }

        SpawnVillagers ();

    }

    void SpawnSelectedVillage () {

        // Start vector is the 0, 0  coordinates on the grid
        // Max vector is the maximum end of the grid , so gridX , GridZ

        Vector3 startVector = vertices[0];
        Vector3 maxVector = new Vector3 (gridX, 0, gridZ);

        RollVillage ();

        // Random Vector gets selected to spawn each village in a random location between start vector and max vector coordinates

        Vector3 randVector = new Vector3 ((Random.Range (startVector.x, maxVector.x)), 0, (Random.Range (startVector.z, maxVector.z)));

        // If statement gathers "returned village" information from RollVillage function (see below)
        // Based on the returned village, a specified village spawn in if the bool is ticked for those villages to spawn in.
        // Villages are grabbed from their object pools and parented to the generator object, as well as added to individual lists. 

        // If a village has been rolled, a bool "villageSpawned" will be set to true, which will initiate the roll function again to generate more villages until the 
        // required amount of villages has been reached.

        if (returnedVillage == "small" && spawnSmall) {

            SmallVillage spawnedVillage;

            spawnedVillage = small_Villages.GetFromObjectPool ();
            spawnedVillage.transform.position = randVector;
            spawnedVillage.transform.SetParent (transform);

            smallVillageCount.Add (spawnedVillage);

            villageSpawned = true;
            if (villageSpawned) {
                RollVillage ();
                villageSpawned = !villageSpawned;
            }

        } else if (returnedVillage == "towerVillage" && spawnTower) {

            TowerVillage towerVillage;

            towerVillage = tower_Villages.GetFromObjectPool ();
            towerVillage.transform.position = randVector;
            towerVillage.transform.SetParent (transform);

            towerVillageCount.Add (towerVillage);

            villageSpawned = true;
            if (villageSpawned) {
                RollVillage ();
                villageSpawned = !villageSpawned;
            }

        } else if (spawnStrange) {

            StrangeVillage strangeVillage;

            strangeVillage = strange_Villages.GetFromObjectPool ();
            strangeVillage.transform.position = randVector;
            strangeVillage.transform.SetParent (transform);

            strangeVillageCount.Add (strangeVillage);

            villageSpawned = true;
            if (villageSpawned) {
                RollVillage ();
                villageSpawned = !villageSpawned;
            }

        }
    }

    void SpawnVillagers () {

        // Spawns NPC's in locations on the grid (spawns are influenced by homeland on VillagerTraits script)

        for (int j = 0; j < people; j++) {

            VillagerTraits newVillager = op_People.GetFromObjectPool ();
            newVillager.SelectTraits ();

            newVillager.transform.SetParent (transform);
            peopleCount.Add (newVillager);

        }
    }

    public void ClearVillages () {

        // Removes villages and npc's to reduce more villages spawning on to
        // Clears all villages and npc's

        ClearVillagers ();

        if (smallVillageCount.Count > 0) {
            for (int i = smallVillageCount.Count - 1; i >= 0; i--) {

                small_Villages.PutIntoObjectPool (smallVillageCount[i]);
                smallVillageCount.RemoveAt (i);

            }
        }

        if (towerVillageCount.Count > 0) {
            for (int i = towerVillageCount.Count - 1; i >= 0; i--) {

                tower_Villages.PutIntoObjectPool (towerVillageCount[i]);
                towerVillageCount.RemoveAt (i);

            }
        }

        if (strangeVillageCount.Count > 0) {
            for (int i = strangeVillageCount.Count - 1; i >= 0; i--) {

                strange_Villages.PutIntoObjectPool (strangeVillageCount[i]);
                strangeVillageCount.RemoveAt (i);

            }
        }
    }

    void ClearVillagers () {

        // Removes all NPC's 

        if (peopleCount.Count > 0) {
            for (int i = peopleCount.Count - 1; i >= 0; i--) {
                op_People.PutIntoObjectPool (peopleCount[i]);
                peopleCount.RemoveAt (i);
            }
        }
    }

    //! PROBABILITY LOGIC
    // Logic controls the probability for a specific village to be spawned in

    public void AddItem () {

        // Adds item to village probability list

        villageValues.Add (new VillageProbability ());
    }

    public void RemoveItem (int i) {

        // Removes item from village probability list

        villageValues.RemoveAt (i);
    }

    public void RollVillage () {

        // Rolls a probability out of a specified total (3 added to a total, each having their own probability (see "VillageProbability"))
        // Gets a random number between 0 and the total and calculates which village gets spawned in based on the number rolled.
        // If a specific village gets rolled too many times, the value gets incremented to increase the chances of spawning a different village based on probability.

        totalProbability = 0;

        for (int i = 0; i < villageValues.Count; i++) {
            totalProbability += villageValues[i].probability + villageValues[i].probabilityLikelinessOffset;
        }

        float randomNum = Random.Range (0, totalProbability);

        VillageProbability selectedVillage = null;
        float runningTotal = 0;
        bool foundCanditate = false;

        for (int i = 0; i < villageValues.Count; i++) {

            bool offsetValue = true;

            if (!foundCanditate) {
                runningTotal += villageValues[i].probability + villageValues[i].probabilityLikelinessOffset;

                if (randomNum <= runningTotal) {
                    selectedVillage = villageValues[i];

                    foundCanditate = true;
                    offsetValue = false;

                    villageValues[i].probabilityLikelinessOffset = 0;
                    villageValues[i].rollCount++;

                    returnedVillage = selectedVillage.villageType.ToString ();

                }
            }

            if (offsetValue) {
                villageValues[i].probabilityLikelinessOffset += villageValues[i].probabilityIncrement;
            }
        }

    }

}