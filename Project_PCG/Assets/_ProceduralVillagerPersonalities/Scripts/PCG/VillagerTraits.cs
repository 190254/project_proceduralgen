using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerTraits : MonoBehaviour {

    // Villager traits script returns values of the villager in order for villager behaviour and spawn to be controlled in future.

    [Header ("Reference")]
    [SerializeField] VillagerSO villagerSO;
    [SerializeField] VillageGenerator generator;

    // Variables that the specified NPC will have.

    [Header ("Variables")]
    [SerializeField] string personality;
    [SerializeField] string homeland;
    [SerializeField] string dish;
    [SerializeField] string backstory;

    public void SelectTraits () {

        generator = GameObject.Find ("==GENERATOR").GetComponent<VillageGenerator> ();
        // Calls function in the scriptable object class

        villagerSO.RandomizeStats ();

        // Selects NPC traits based on values returned from the Scriptable object class

        personality = villagerSO.persona;
        homeland = villagerSO.home;
        dish = villagerSO.prefDish;

        SetVillagerSpawn ();

    }

    //! Villager Spawn Test
    // NOT THE FINAL WAY THIS WILL BE DONE
    void SetVillagerSpawn () {

        // Based on returned value of homeland within villager SO class
        // Change villager position to quadrant of the grid

        switch (homeland) {

            // Lower left Quadrant
            // 0,0 - gridX/ 2 , GridZ / 2
            case "0":
                Vector3 firstStartVector = new Vector3 (0, 0, 0);
                Vector3 firstMaxVector = new Vector3 (generator.gridX / 2, 0, generator.gridZ / 2);

                Vector3 firstSpawn = new Vector3 (Random.Range (firstStartVector.x, firstMaxVector.x), 0, Random.Range (firstStartVector.z, firstMaxVector.z));

                transform.position = firstSpawn;

                break;
                // Upper Left Quadrant
                // 0 , GridZ  - gridX/ 2 , gridZ
            case "1":
                Vector3 secondStartVector = new Vector3 (0, 0, generator.gridZ / 2);
                Vector3 secondMaxVector = new Vector3 (generator.gridX / 2, 0, generator.gridZ);

                Vector3 secondSpawn = new Vector3 (Random.Range (secondStartVector.x, secondMaxVector.x), 0, Random.Range (secondStartVector.z, secondMaxVector.z));

                transform.position = secondSpawn;

                break;
                // Lower Right quadrant
                // GridX , 0 - GridX , GridZ / 2 
            case "2":
                Vector3 thirdStartVector = new Vector3 (generator.gridX / 2, 0, 0);
                Vector3 thirdMaxVector = new Vector3 (generator.gridX, 0, generator.gridZ / 2);

                Vector3 thirdSpawn = new Vector3 (Random.Range (thirdStartVector.x, thirdMaxVector.x), 0, Random.Range (thirdStartVector.z, thirdMaxVector.z));

                transform.position = thirdSpawn;

                break;

        }

    }

}