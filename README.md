# Procedurally Generated NPC Traits

**This project mainly focuses on the procedural generation of the following:** 

1. NPC Personality Traits
2. NPC's Homelands.
3. NPC's Favourite Dishes/Prefered Dishes. (Mainly based on Medieval Cook, but is subject to change)
4. Small Backstories for NPC's.

**How it works: **

1. The algorithm generates a procedural grid with numbers specified in variables GridX & GridY on the VillageGenerator script.
2. After generating a grid, the player/user can select a number of villages and villagers to generate within the environment.
3. Villages and Villagers will be spread out based on the grid's given values. 
4. Generated NPC's will then be given random traits, as has been listed above.


**What the user can change: **

* Grid Size
* Village & Villager frequency
* All villager traits in the VillagerSO script (Based on Enums that get selected randomly in lists).
* Types of villages and probability for those villages to spawn in.

_To add or remove specific traits of a villager in VillagerSO Script : _

1. Open Villager SO script and add or remove enum value.
2. Go to SO folder and click on the villager scriptable object to show it in the inspector.
3. In the inspector , open the list that you have changed the enum value of.
4. Add a new item to the list and select the enum you have made OR remove the item which you have removed within the VillagerSO script.

_To add or remove a specific village from the probability window: _

1. Remove the item from the list under the probability tab in the VillageGenerator script.
2. Remove the object pool reference inside of the VillageGenerator script. 

To add your own villages:

1. Create a new village prefab.
2. Create a script on the new village prefab (or change existing prefabs)
3. Rename or add a new object pool object and place the object pool script on the object, and drag the prefab in the empty slot.
4. In VillageGenerator, add/remove/rename VillageType enum values. 
5. In VillageGenerator go to the SpawnVillages function and add your own logic for the village or rename already existing logic. 
6. In  VillageGenerator in the inspector, navigate to the probability tab and add/remove items from the list and add values for probability.

You're done! 

**Enjoy the tool.**


