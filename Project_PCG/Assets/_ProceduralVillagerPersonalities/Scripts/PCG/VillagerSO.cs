using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu (fileName = "VillagerSO", menuName = "Project_PCG/VillagerSO", order = 0)]
public class VillagerSO : ScriptableObject {

    /*
     Scriptable Object script that contains all the data for the villagers
     This includes :  Personality, Favourite Dishes, Homelands and Backstories

     NB! Since backstories do not influence other variables within the villager, it is interchangable at will.
                     
    */

    // Enums contain all the data to select the different variables for the villagers
    // User can add or remove the names within the enums as needed due to the fact that lists will randomize the selected value

    public enum Personalities {

        Amiable,
        Analytical,
        Driver,
        Expressive

    }
    public enum Homelands {

        Alair,
        Riverstead,
        Hovengrove

    }
    public enum Dishes {

        WolfSteak,
        YourMomJokes,
        KiwiSpecial

    }

    Personalities personality;
    Homelands homeland;
    Dishes dish;

    // Lists to control enums

    [Header ("Variables")]

    public List<Personalities> personas = new List<Personalities> ();
    public List<Homelands> homes = new List<Homelands> ();
    public List<Dishes> prefDishes = new List<Dishes> ();

    // Story

    [TextArea (3, 10)]
    public List<string> backstories = new List<string> ();

    // Variables

    [Header ("Displayed Variables")]

    // NPC Personality
    public string persona;

    // NPC Homeland
    public string home;

    // NPC Favourite Dish
    public string prefDish;

    public void RandomizeStats () {

        // Randomizes the enums contained within the lists and returns a string value of the selected enum for the villager

        int randomPersona = Random.Range (0, personas.Count);
        int randomHome = Random.Range (0, homes.Count);
        int randomDish = Random.Range (0, prefDishes.Count);;

        // Returned values

        persona = randomPersona.ToString ();
        home = randomHome.ToString ();
        prefDish = randomDish.ToString ();

    }

}